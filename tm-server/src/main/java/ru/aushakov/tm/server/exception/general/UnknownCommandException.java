package ru.aushakov.tm.server.exception.general;

import org.jetbrains.annotations.Nullable;

public class UnknownCommandException extends RuntimeException {

    public UnknownCommandException(@Nullable final String command) {
        super("Command '" + command + "' is not supported");
    }

}
