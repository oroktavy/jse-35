package ru.aushakov.tm.server.exception.empty;

public class EmptyPropertyException extends RuntimeException {

    public EmptyPropertyException() {
        super("No property passed to read from config!");
    }

}
