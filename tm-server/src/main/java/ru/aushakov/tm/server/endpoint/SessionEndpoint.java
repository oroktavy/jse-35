package ru.aushakov.tm.server.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint {

    @WebMethod
    public Session openSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        return sessionService.open(login, password);
    }

    @WebMethod
    public void closeSession(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        sessionService.close(session);
    }

}
