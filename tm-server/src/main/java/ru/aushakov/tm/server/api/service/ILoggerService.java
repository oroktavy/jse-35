package ru.aushakov.tm.server.api.service;

public interface ILoggerService {

    void info(String message);

    void command(String message);

    void error(Exception e);

}
