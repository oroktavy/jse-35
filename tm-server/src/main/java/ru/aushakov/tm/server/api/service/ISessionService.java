package ru.aushakov.tm.server.api.service;

import ru.aushakov.tm.server.model.Session;

public interface ISessionService {

    Session open(String login, String password);

    void close(Session session);

    void validate(Session session);

    void validateAdmin(Session session);

    String login(String login, String password);

    void changePassword(String oldPassword, String newPassword, String userId);

    void closeAllSessions();

}
