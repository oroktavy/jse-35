package ru.aushakov.tm.server.repository;

import ru.aushakov.tm.server.api.repository.IProjectRepository;
import ru.aushakov.tm.server.model.Project;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {
}
