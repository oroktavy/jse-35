package ru.aushakov.tm.server.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.exception.empty.EmptySessionException;
import ru.aushakov.tm.server.model.Session;
import ru.aushakov.tm.server.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@WebService
public final class UserEndpoint extends AbstractEndpoint {

    @WebMethod
    public User addUser(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        return userService.add(login, password, email);
    }

    @WebMethod
    public void changePassword(
            @WebParam(name = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        sessionService.changePassword(oldPassword, newPassword, userId);
        sessionService.close(session);
    }

    @WebMethod
    public User viewProfile(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return userService.findOneById(userId);
    }

}
