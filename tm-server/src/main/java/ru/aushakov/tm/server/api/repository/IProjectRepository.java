package ru.aushakov.tm.server.api.repository;

import ru.aushakov.tm.server.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
