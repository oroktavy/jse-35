package ru.aushakov.tm.server.exception.empty;

public class EmptyProjectIdException extends RuntimeException {

    public EmptyProjectIdException() {
        super("Provided project id is empty!");
    }

}
