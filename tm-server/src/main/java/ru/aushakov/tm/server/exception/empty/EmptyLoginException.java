package ru.aushakov.tm.server.exception.empty;

public class EmptyLoginException extends RuntimeException {

    public EmptyLoginException() {
        super("Provided login is empty!");
    }

}
