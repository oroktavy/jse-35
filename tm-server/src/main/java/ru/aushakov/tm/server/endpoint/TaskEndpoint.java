package ru.aushakov.tm.server.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.enumerated.SortType;
import ru.aushakov.tm.server.exception.empty.EmptySessionException;
import ru.aushakov.tm.server.model.Session;
import ru.aushakov.tm.server.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public final class TaskEndpoint extends AbstractEndpoint {

    @WebMethod
    public List<Task> findAllTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.findAll(userId);
    }

    @WebMethod
    public List<Task> findAllSortedTasks(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sort") @Nullable final String sort
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        @Nullable final SortType sortType = SortType.toSortType(sort);
        if (sortType == null) return taskService.findAll(userId);
        else return taskService.findAll(userId, sortType.getComparator());
    }

    @WebMethod
    public void clearTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        taskService.clear(userId);
    }

    @WebMethod
    public Task findTaskById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.findOneById(id, userId);
    }

    @WebMethod
    public Task findTaskByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.findOneByIndex(index, userId);
    }

    @WebMethod
    public Task findTaskByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.findOneByName(name, userId);
    }

    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.removeOneById(id, userId);
    }

    @WebMethod
    public Task removeTaskByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.removeOneByIndex(index, userId);
    }

    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.removeOneByName(name, userId);
    }

    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.updateOneById(id, userId, name, description);
    }

    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.updateOneByIndex(index, userId, name, description);
    }

    @WebMethod
    public Task startTaskById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.startOneById(id, userId);
    }

    @WebMethod
    public Task startTaskByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.startOneByIndex(index, userId);
    }

    @WebMethod
    public Task startTaskByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.startOneByName(name, userId);
    }

    @WebMethod
    public Task finishTaskById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.finishOneById(id, userId);
    }

    @WebMethod
    public Task finishTaskByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.finishOneByIndex(index, userId);
    }

    @WebMethod
    public Task finishTaskByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.finishOneByName(name, userId);
    }

    @WebMethod
    public Task addTask(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.add(name, description, userId);
    }

    @WebMethod
    public Task changeTaskStatusById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "statusId") @Nullable final String statusId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.changeOneStatusById(id, statusId, userId);
    }

    @WebMethod
    public Task changeTaskStatusByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "statusId") @Nullable final String statusId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.changeOneStatusByIndex(index, statusId, userId);
    }

    @WebMethod
    public Task changeTaskStatusByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "statusId") @Nullable final String statusId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.changeOneStatusByName(name, statusId, userId);
    }

    @WebMethod
    public Task assignTaskToProject(
            @WebParam(name = "taskId") @Nullable final String taskId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.assignTaskToProject(taskId, projectId, userId);
    }

    @WebMethod
    public Task unbindTaskFromProject(
            @WebParam(name = "taskId") @Nullable final String taskId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.unbindTaskFromProject(taskId, userId);
    }

    @WebMethod
    public List<Task> findAllTasksByProjectId(
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return taskService.findAllTasksByProjectId(projectId, userId);
    }

}
