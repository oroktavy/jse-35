package ru.aushakov.tm.server.api;

import ru.aushakov.tm.server.api.service.*;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    IAuthService getAuthService();

    IDataService getDataService();

    IPropertyService getPropertyService();

    ISessionService getSessionService();

}
