package ru.aushakov.tm.server.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.dto.Domain;
import ru.aushakov.tm.server.enumerated.ConfigProperty;
import ru.aushakov.tm.server.model.Session;
import ru.aushakov.tm.server.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminEndpoint extends AbstractEndpoint {

    @WebMethod
    public User removeUserByLogin(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        return userService.removeOneByLogin(login);
    }

    @WebMethod
    public User removeOneById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        return userService.removeOneById(id);
    }

    @WebMethod
    public void addUserWithRole(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "roleId") @Nullable final String roleId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        userService.add(login, password, email, roleId);
    }

    @WebMethod
    public User setPassword(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        return userService.setPassword(login, newPassword);
    }

    @WebMethod
    public User updateUserById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        return userService.updateOneById(id, lastName, firstName, middleName);
    }

    @WebMethod
    public User updateUserByLogin(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        return userService.updateOneByLogin(login, lastName, firstName, middleName);
    }

    @WebMethod
    public User lockUserByLogin(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        return userService.lockOneByLogin(login);
    }

    @WebMethod
    public User unlockUserByLogin(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        return userService.unlockOneByLogin(login);
    }

    @WebMethod
    public String getDataFileName(
            @WebParam(name = "format") @Nullable final String format,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        @Nullable final ConfigProperty configProperty = ConfigProperty.toConfigProperty(format);
        if (configProperty == null) return null;
        switch (configProperty) {
            case DATAFILE_BINARY:
                return dataService.getFileBinary();
            case DATAFILE_BASE64:
                return dataService.getFileBase64();
            case DATAFILE_JSON:
                return dataService.getFileJson();
            case DATAFILE_XML:
                return dataService.getFileXml();
            case DATAFILE_JAXB_XML:
                return dataService.getFileXmlForJaxB();
            default:
                return null;
        }
    }

    @WebMethod
    public Domain getDomain(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        return dataService.getDomain();
    }

    @WebMethod
    public void setDomain(
            @WebParam(name = "domain") @Nullable final Domain domain,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validateAdmin(session);
        dataService.setDomain(domain);
        sessionService.closeAllSessions();
    }

}
