package ru.aushakov.tm.server.api.service;

import ru.aushakov.tm.server.api.repository.IBusinessRepository;
import ru.aushakov.tm.server.model.AbstractBusinessEntity;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IBusinessRepository<E> {

    E add(String name, String description, String userId);

    E changeOneStatusById(String id, String statusId, String userId);

    E changeOneStatusByIndex(Integer index, String statusId, String userId);

    E changeOneStatusByName(String name, String statusId, String userId);

}
