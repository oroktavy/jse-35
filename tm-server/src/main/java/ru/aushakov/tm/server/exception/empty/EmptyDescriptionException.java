package ru.aushakov.tm.server.exception.empty;

public class EmptyDescriptionException extends RuntimeException {

    public EmptyDescriptionException() {
        super("Provided description is empty!");
    }

}
