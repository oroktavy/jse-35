package ru.aushakov.tm.server.dto.adapter;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.enumerated.Role;
import ru.aushakov.tm.server.model.AbstractEntity;

@Setter
@Getter
public class AdaptedUser extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String lastName;

    @Nullable
    private String firstName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    private boolean lockedFlag = false;

}
