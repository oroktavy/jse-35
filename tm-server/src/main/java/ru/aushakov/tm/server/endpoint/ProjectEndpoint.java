package ru.aushakov.tm.server.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.enumerated.SortType;
import ru.aushakov.tm.server.exception.empty.EmptySessionException;
import ru.aushakov.tm.server.model.Project;
import ru.aushakov.tm.server.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint {

    @WebMethod
    public List<Project> findAllProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.findAll(userId);
    }

    @WebMethod
    public List<Project> findAllSortedProjects(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sort") @Nullable final String sort
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        @Nullable final SortType sortType = SortType.toSortType(sort);
        if (sortType == null) return projectService.findAll(userId);
        else return projectService.findAll(userId, sortType.getComparator());
    }

    @WebMethod
    public void clearProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        projectService.clear(userId);
    }

    @WebMethod
    public Project findProjectById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.findOneById(id, userId);
    }

    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.findOneByIndex(index, userId);
    }

    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.findOneByName(name, userId);
    }

    @WebMethod
    public Project removeProjectById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.removeOneById(id, userId);
    }

    @WebMethod
    public Project removeProjectByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.removeOneByIndex(index, userId);
    }

    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.removeOneByName(name, userId);
    }

    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.updateOneById(id, userId, name, description);
    }

    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.updateOneByIndex(index, userId, name, description);
    }

    @WebMethod
    public Project startProjectById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.startOneById(id, userId);
    }

    @WebMethod
    public Project startProjectByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.startOneByIndex(index, userId);
    }

    @WebMethod
    public Project startProjectByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.startOneByName(name, userId);
    }

    @WebMethod
    public Project finishProjectById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.finishOneById(id, userId);
    }

    @WebMethod
    public Project finishProjectByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.finishOneByIndex(index, userId);
    }

    @WebMethod
    public Project finishProjectByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.finishOneByName(name, userId);
    }

    @WebMethod
    public Project addProject(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.add(name, description, userId);
    }

    @WebMethod
    public Project changeProjectStatusById(
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "statusId") @Nullable final String statusId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.changeOneStatusById(id, statusId, userId);
    }

    @WebMethod
    public Project changeProjectStatusByIndex(
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "statusId") @Nullable final String statusId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.changeOneStatusByIndex(index, statusId, userId);
    }

    @WebMethod
    public Project changeProjectStatusByName(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "statusId") @Nullable final String statusId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.changeOneStatusByName(name, statusId, userId);
    }

    @WebMethod
    public Project deepDeleteProjectById(
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        @Nullable final String userId = Optional.ofNullable(session)
                .orElseThrow(EmptySessionException::new).getUserId();
        return projectService.deepDeleteProjectById(projectId, userId);
    }

}
