package ru.aushakov.tm.server.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.aushakov.tm.server.dto.adapter.AdaptedUser;
import ru.aushakov.tm.server.model.Project;
import ru.aushakov.tm.server.model.User;

import java.util.List;

public class UserRepositoryTest {

    @Nullable
    private UserRepository userRepository;

    @Nullable
    private User user;

    @Before
    public void before() {
        userRepository = new UserRepository();
        user = new User("User", "554");
        userRepository.add(user);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("User", user.getLogin());

        @NotNull final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user, userById);
    }

    @Test
    public void findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final User user = userRepository.findOneById("34");
        Assert.assertNull(user);
    }

    @Test
    public void findByIdNull() {
        @NotNull final User user = userRepository.findOneById(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByIdIncorrectUser() {
        @NotNull final User user = userRepository.findOneById(this.user.getId());
        Assert.assertNull(user);
    }

    @Test
    public void remove() {
        userRepository.removeOneById(user.getId());
        Assert.assertNull(userRepository.findOneById(user.getId()));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertNull(userRepository.removeOneById(null));
    }

    @Test
    public void findByLogin() {
        @NotNull final User user = userRepository.findOneByLogin("User");
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLoginIncorrect() {
        @NotNull final User user = userRepository.findOneByLogin("34");
        Assert.assertNull(user);
    }

    @Test
    public void findByNameNull() {
        @NotNull final User user = userRepository.findOneByLogin(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByIndex() {
        @NotNull final User user = userRepository.findOneByIndex(0);
        Assert.assertNotNull(user);
    }

    @Test
    public void removeById() {
        userRepository.removeOneById(user.getId());
        Assert.assertNull(userRepository.findOneById(user.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(userRepository.removeOneById(null));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final User user = userRepository.removeOneById("34");
        Assert.assertNull(user);
    }

    @Test
    public void removeByIndex() {
        @NotNull final User user = userRepository.removeOneByIndex(0);
        Assert.assertNotNull(user);
    }

}
