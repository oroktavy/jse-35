package ru.aushakov.tm.server.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.aushakov.tm.server.model.Task;

import java.util.List;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;

    @Nullable
    private Task task;

    @Before
    public void before() {
        taskRepository = new TaskRepository();
        task = new Task();
        task.setName("Task");
        task.setUserId("1234");
        taskRepository.add(task);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NotNull final Task taskById = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task, taskById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskRepository.findAll("testUser");
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskRepository.findAll("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Task task = taskRepository.findOneById("34");
        Assert.assertNull(task);
    }

    @Test
    public void findByIdNull() {
        @NotNull final Task task = taskRepository.findOneById(null);
        Assert.assertNull(task);
    }

    @Test
    public void findByIdIncorrectUser() {
        @NotNull final Task task = taskRepository.findOneById(this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void remove() {
        taskRepository.removeOneById(task.getId());
        Assert.assertNull(taskRepository.findOneById(task.getId()));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertNull(taskRepository.removeOneById(null));
    }

    @Test
    public void findByName() {
        @NotNull final Task task = taskRepository.findOneByName("Task", "1234");
        Assert.assertNotNull(task);
    }

    @Test
    public void findByNameIncorrect() {
        @NotNull final Task task = taskRepository.findOneByName("34", "1234");
        Assert.assertNull(task);
    }

    @Test
    public void findByNameNull() {
        @NotNull final Task task = taskRepository.findOneByName(null, "1234");
        Assert.assertNull(task);
    }

    @Test
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskRepository.findOneByName(this.task.getName(), "1");
        Assert.assertNull(task);
    }

    @Test
    public void findByIndex() {
        @NotNull final Task task = taskRepository.findOneByIndex(0, "1234");
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        taskRepository.removeOneById(task.getId(), "1234");
        Assert.assertNull(taskRepository.findOneById(task.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(taskRepository.removeOneById(null, "1234"));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Task task = taskRepository.removeOneById("34", "1234");
        Assert.assertNull(task);
    }

    @Test
    public void removeByIdIncorrectUser() {
        @NotNull final Task task = taskRepository.removeOneById(this.task.getId(), "1234");
        Assert.assertNull(task);
    }

    @Test
    public void removeByIndex() {
        @NotNull final Task task = taskRepository.removeOneByIndex(0, "1234");
        Assert.assertNotNull(task);
    }

    @Test
    public void removeByName() {
        @NotNull final Task task = taskRepository.removeOneByName("Task", "1234");
        Assert.assertNotNull(task);
    }

    @Test
    public void removeByNameIncorrect() {
        @NotNull final Task task = taskRepository.removeOneByName("34", "1234");
        Assert.assertNull(task);
    }

    @Test
    public void removeByNameNull() {
        @NotNull final Task task = taskRepository.removeOneByName(null, "1234");
        Assert.assertNull(task);
    }

    @Test
    public void removeByNameIncorrectUser() {
        @NotNull final Task task = taskRepository.removeOneByName(this.task.getName(), "1234");
        Assert.assertNull(task);
    }

}
