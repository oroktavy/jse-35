package ru.aushakov.tm.server.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.aushakov.tm.server.model.Project;

import java.util.List;

public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private Project project;

    @Before
    public void before() {
        projectRepository = new ProjectRepository();
        project = new Project();
        project.setName("Project");
        project.setUserId("1234");
        projectRepository.add(project);
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NotNull final Project projectById = projectRepository.findOneById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectRepository.findAll("testUser");
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectRepository.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Project project = projectRepository.findOneById("34");
        Assert.assertNull(project);
    }

    @Test
    public void findByIdNull() {
        @NotNull final Project project = projectRepository.findOneById(null);
        Assert.assertNull(project);
    }

    @Test
    public void findByIdIncorrectUser() {
        @NotNull final Project project = projectRepository.findOneById(this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void remove() {
        projectRepository.removeOneById(project.getId());
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertNull(projectRepository.removeOneById(null));
    }

    @Test
    public void findByName() {
        @NotNull final Project project = projectRepository.findOneByName("Project", "1234");
        Assert.assertNotNull(project);
    }

    @Test
    public void findByNameIncorrect() {
        @NotNull final Project project = projectRepository.findOneByName("34", "1234");
        Assert.assertNull(project);
    }

    @Test
    public void findByNameNull() {
        @NotNull final Project project = projectRepository.findOneByName(null, "1234");
        Assert.assertNull(project);
    }

    @Test
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectRepository.findOneByName(this.project.getName(), "1");
        Assert.assertNull(project);
    }

    @Test
    public void findByIndex() {
        @NotNull final Project project = projectRepository.findOneByIndex(0, "1234");
        Assert.assertNotNull(project);
    }

    @Test
    public void removeById() {
        projectRepository.removeOneById(project.getId(), "1234");
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(projectRepository.removeOneById(null, "1234"));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Project project = projectRepository.removeOneById("34", "1234");
        Assert.assertNull(project);
    }

    @Test
    public void removeByIdIncorrectUser() {
        @NotNull final Project project = projectRepository.removeOneById(this.project.getId(), "1234");
        Assert.assertNull(project);
    }

    @Test
    public void removeByIndex() {
        @NotNull final Project project = projectRepository.removeOneByIndex(0, "1234");
        Assert.assertNotNull(project);
    }

    @Test
    public void removeByName() {
        @NotNull final Project project = projectRepository.removeOneByName("Project", "1234");
        Assert.assertNotNull(project);
    }

    @Test
    public void removeByNameIncorrect() {
        @NotNull final Project project = projectRepository.removeOneByName("34", "1234");
        Assert.assertNull(project);
    }

    @Test
    public void removeByNameNull() {
        @NotNull final Project project = projectRepository.removeOneByName(null, "1234");
        Assert.assertNull(project);
    }

    @Test
    public void removeByNameIncorrectUser() {
        @NotNull final Project project = projectRepository.removeOneByName(this.project.getName(), "1234");
        Assert.assertNull(project);
    }

}
