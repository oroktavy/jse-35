package ru.aushakov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.aushakov.tm.client.endpoint.*;
import ru.aushakov.tm.client.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Nullable
    private static Session session;

    @Nullable
    private Project project;

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession("TEST", "TEST");
    }

    @Before
    public void before() {
        project = projectEndpoint.addProject("Test", "1111111", session);
    }

    @After
    public void after() {
        projectEndpoint.removeProjectById(project.getId(), session);
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Test", project.getName());

        @NotNull final Project projectById = projectEndpoint.findProjectById(project.getId(), session);
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        @NotNull final List<Project> projects = projectEndpoint.findAllProjects(session);
        Assert.assertTrue(projects.size() > 0);
    }


    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findAllByUserIdIncorrect() {
        projectEndpoint.findAllProjects(new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void findById() {
        @NotNull final Project project = projectEndpoint.findProjectById(this.project.getId(), session);
        Assert.assertNotNull(project);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Project project = projectEndpoint.findProjectById("34", session);
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdNull() {
        projectEndpoint.findProjectById(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdIncorrectUser() {
        projectEndpoint.findProjectById(this.project.getId(), new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByName() {
        @NotNull final Project project = projectEndpoint.findProjectByName("Test", session);
        Assert.assertNotNull(project);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Project project = projectEndpoint.findProjectByName("34", session);
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameNull() {
        projectEndpoint.findProjectByName(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameIncorrectUser() {
        projectEndpoint.findProjectByName(this.project.getName(), new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIndex() {
        @NotNull final Project project = projectEndpoint.findProjectByIndex(0, session);
        Assert.assertNotNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrect() {
        projectEndpoint.findProjectByIndex(34, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexNull() {
        projectEndpoint.findProjectByIndex(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrectUser() {
        projectEndpoint.findProjectByIndex(0, new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeById() {
        projectEndpoint.removeProjectById(project.getId(), session);
        Assert.assertNull(projectEndpoint.findProjectById(project.getId(), session));
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdNull() {
        Assert.assertNull(projectEndpoint.removeProjectById(null, session));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdIncorrect() {
        @NotNull final Project project = projectEndpoint.removeProjectById("34", session);
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdIncorrectUser() {
        projectEndpoint.removeProjectById(this.project.getId(), new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIndex() {
        @NotNull final Project project = projectEndpoint.removeProjectByIndex(0, session);
        Assert.assertNotNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexIncorrect() {
        projectEndpoint.removeProjectByIndex(34, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexNull() {
        projectEndpoint.removeProjectByIndex(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexIncorrectUser() {
        projectEndpoint.removeProjectByIndex(0, new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByName() {
        @NotNull final Project project = projectEndpoint.removeProjectByName("Test", session);
        Assert.assertNotNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameIncorrect() {
        projectEndpoint.removeProjectByName("34", session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameNull() {
        projectEndpoint.removeProjectByName(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameIncorrectUser() {
        projectEndpoint.removeProjectByName(this.project.getName(), new Session());
    }

}
