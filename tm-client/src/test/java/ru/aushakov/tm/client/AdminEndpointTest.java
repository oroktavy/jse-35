package ru.aushakov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.aushakov.tm.client.endpoint.*;
import ru.aushakov.tm.client.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class AdminEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @Nullable
    private Session session;

    @Before
    public void before() {
        session = sessionEndpoint.openSession("ADMIN", "ADMIN");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void lockByLogin() {
        adminEndpoint.lockUserByLogin("TEST", session);
        sessionEndpoint.openSession("TEST", "TEST");
    }

    @Test
    @Category(SoapCategory.class)
    public void unlockByLogin() {
        adminEndpoint.lockUserByLogin("TEST", session);
        adminEndpoint.unlockUserByLogin("TEST", session);
        sessionEndpoint.openSession("TEST", "TEST");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void registerIncorrect() {
        adminEndpoint.addUserWithRole("user", "user", "email", "ADMIN", session);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUser() {
        @NotNull final AdaptedUser user = adminEndpoint.updateUserByLogin(
                "TEST", "l", "f", "m", session
        );
        Assert.assertNotNull(user);
        Assert.assertEquals("f", user.getFirstName());
        Assert.assertEquals("l", user.getLastName());
        Assert.assertEquals("m", user.getMiddleName());
    }

}
