package ru.aushakov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.aushakov.tm.client.endpoint.AdaptedUser;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.endpoint.SessionEndpoint;
import ru.aushakov.tm.client.endpoint.SessionEndpointService;
import ru.aushakov.tm.client.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Nullable
    private Session session;

    @Before
    public void before() {
        session = sessionEndpoint.openSession("user", "user");
    }

    @Test
    public void open() {
        Assert.assertNotNull(session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void openIncorrect() {
        @NotNull final Session session = sessionEndpoint.openSession("test", "test1");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void close() {
        sessionEndpoint.closeSession(session);
        sessionEndpoint.closeSession(session);
    }

}
