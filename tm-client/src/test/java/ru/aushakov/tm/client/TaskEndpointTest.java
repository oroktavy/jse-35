package ru.aushakov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.aushakov.tm.client.endpoint.*;
import ru.aushakov.tm.client.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class TaskEndpointTest {

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @Nullable
    private static Session session;

    @Nullable
    private Task task;

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession("TEST", "TEST");
    }

    @Before
    public void before() {
        task = taskEndpoint.addTask("Test", "22222222", session);
    }

    @After
    public void after() {
        taskEndpoint.removeTaskById(task.getId(), session);
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Test", task.getName());

        @NotNull final Task taskById = taskEndpoint.findTaskById(task.getId(), session);
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        @NotNull final List<Task> tasks = taskEndpoint.findAllTasks(session);
        Assert.assertTrue(tasks.size() > 0);
    }


    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findAllByUserIdIncorrect() {
        taskEndpoint.findAllTasks(new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void findById() {
        @NotNull final Task task = taskEndpoint.findTaskById(this.task.getId(), session);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Task task = taskEndpoint.findTaskById("34", session);
        Assert.assertNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdNull() {
        taskEndpoint.findTaskById(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdIncorrectUser() {
        taskEndpoint.findTaskById(this.task.getId(), new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByName() {
        @NotNull final Task task = taskEndpoint.findTaskByName("Test", session);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Task task = taskEndpoint.findTaskByName("34", session);
        Assert.assertNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameNull() {
        taskEndpoint.findTaskByName(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameIncorrectUser() {
        taskEndpoint.findTaskByName(this.task.getName(), new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIndex() {
        @NotNull final Task task = taskEndpoint.findTaskByIndex(0, session);
        Assert.assertNotNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrect() {
        taskEndpoint.findTaskByIndex(34, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexNull() {
        taskEndpoint.findTaskByIndex(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrectUser() {
        taskEndpoint.findTaskByIndex(0, new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeById() {
        taskEndpoint.removeTaskById(task.getId(), session);
        Assert.assertNull(taskEndpoint.findTaskById(task.getId(), session));
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdNull() {
        Assert.assertNull(taskEndpoint.removeTaskById(null, session));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdIncorrect() {
        @NotNull final Task task = taskEndpoint.removeTaskById("34", session);
        Assert.assertNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdIncorrectUser() {
        taskEndpoint.removeTaskById(this.task.getId(), new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIndex() {
        @NotNull final Task task = taskEndpoint.removeTaskByIndex(0, session);
        Assert.assertNotNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexIncorrect() {
        taskEndpoint.removeTaskByIndex(34, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexNull() {
        taskEndpoint.removeTaskByIndex(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexIncorrectUser() {
        taskEndpoint.removeTaskByIndex(0, new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByName() {
        @NotNull final Task task = taskEndpoint.removeTaskByName("Test", session);
        Assert.assertNotNull(task);
    }

    @Category(SoapCategory.class)
    @Test
    public void removeByNameIncorrect() {
        Assert.assertNull(taskEndpoint.removeTaskByName("34", session));
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameNull() {
        taskEndpoint.removeTaskByName(null, session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameIncorrectUser() {
        taskEndpoint.removeTaskByName(this.task.getName(), new Session());
    }

}
