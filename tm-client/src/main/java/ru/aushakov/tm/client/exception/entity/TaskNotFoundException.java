package ru.aushakov.tm.client.exception.entity;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Task not found!");
    }

}
