package ru.aushakov.tm.client.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.command.AbstractCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;

public final class LogoutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_LOGOUT;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final Session session = authService.getSession();
        serviceLocator.getSessionService().getSessionEndpointPort().closeSession(session);
        authService.setSession(null);
    }

}
