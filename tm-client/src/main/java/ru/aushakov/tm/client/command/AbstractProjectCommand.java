package ru.aushakov.tm.client.command;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.endpoint.Project;
import ru.aushakov.tm.client.exception.entity.ProjectNotFoundException;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("STATUS: " + project.getStatus());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("CREATED: " + project.getCreated());
        System.out.println("START DATE: " + project.getStartDate());
        System.out.println("END DATE: " + project.getEndDate());
    }

}
