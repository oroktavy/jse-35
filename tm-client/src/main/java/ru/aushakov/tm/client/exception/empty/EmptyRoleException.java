package ru.aushakov.tm.client.exception.empty;

public class EmptyRoleException extends RuntimeException {

    public EmptyRoleException() {
        super("Empty role encountered in list!");
    }

}
