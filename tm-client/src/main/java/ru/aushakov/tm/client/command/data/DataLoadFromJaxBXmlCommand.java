package ru.aushakov.tm.client.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.command.AbstractDataCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.AdminEndpoint;
import ru.aushakov.tm.client.endpoint.Domain;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.ConfigProperty;
import ru.aushakov.tm.client.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;

public final class DataLoadFromJaxBXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_LOAD_FROM_JAXB_XML;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load application data from xml format with JaxB";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM XML WITH JAXB]");
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final Session session = authService.getSession();
        @NotNull final AdminEndpoint dataService = serviceLocator.getDataService().getAdminEndpointPort();
        @NotNull final String fileXml = dataService.getDataFileName(ConfigProperty.DATAFILE_JAXB_XML.name(), session);

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final FileInputStream fileInputStream = new FileInputStream(fileXml);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(fileInputStream);
        dataService.setDomain(domain, session);
        authService.setSession(null);
    }

}
