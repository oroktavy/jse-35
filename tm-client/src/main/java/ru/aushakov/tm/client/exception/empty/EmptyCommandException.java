package ru.aushakov.tm.client.exception.empty;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractCommand;

public class EmptyCommandException extends RuntimeException {

    public EmptyCommandException() {
        super("No command provided!");
    }

    public EmptyCommandException(@Nullable final AbstractCommand command) {
        super("Can not register/operate on empty command!");
    }

}
