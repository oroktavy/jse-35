package ru.aushakov.tm.client.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractTaskCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.endpoint.Task;
import ru.aushakov.tm.client.endpoint.TaskEndpoint;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.enumerated.SortType;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.TASK_LIST;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        System.out.println("[TASK LIST]");
        List<Task> tasks;
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + " OR PRESS ENTER:");
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final TaskEndpoint taskService = serviceLocator.getTaskService().getTaskEndpointPort();
        tasks = taskService.findAllSortedTasks(session, sortType);
        if (tasks == null || tasks.size() < 1) {
            System.out.println("[NOTHING FOUND]");
            return;
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
