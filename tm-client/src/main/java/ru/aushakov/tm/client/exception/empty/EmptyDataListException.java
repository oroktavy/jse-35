package ru.aushakov.tm.client.exception.empty;

public class EmptyDataListException extends RuntimeException {

    public EmptyDataListException() {
        super("Empty data list provided!");
    }

}
