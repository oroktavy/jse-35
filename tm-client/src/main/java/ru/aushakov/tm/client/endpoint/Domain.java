
package ru.aushakov.tm.client.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * &lt;p&gt;Java class for domain complex type.
 * <p>
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * <p>
 * &lt;pre&gt;
 * &amp;lt;complexType name="domain"&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="domainDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 * &amp;lt;element name="projects" minOccurs="0"&amp;gt;
 * &amp;lt;complexType&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="project" type="{http://endpoint.server.tm.aushakov.ru/}project" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &amp;lt;/element&amp;gt;
 * &amp;lt;element name="tasks" minOccurs="0"&amp;gt;
 * &amp;lt;complexType&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="task" type="{http://endpoint.server.tm.aushakov.ru/}task" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &amp;lt;/element&amp;gt;
 * &amp;lt;element name="users" minOccurs="0"&amp;gt;
 * &amp;lt;complexType&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="user" type="{http://endpoint.server.tm.aushakov.ru/}adaptedUser" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &amp;lt;/element&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "domain", propOrder = {
        "domainDate",
        "projects",
        "tasks",
        "users"
})
public class Domain {

    protected String domainDate;
    protected Domain.Projects projects;
    protected Domain.Tasks tasks;
    protected Domain.Users users;

    /**
     * Gets the value of the domainDate property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDomainDate() {
        return domainDate;
    }

    /**
     * Sets the value of the domainDate property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDomainDate(String value) {
        this.domainDate = value;
    }

    /**
     * Gets the value of the projects property.
     *
     * @return possible object is
     * {@link Domain.Projects }
     */
    public Domain.Projects getProjects() {
        return projects;
    }

    /**
     * Sets the value of the projects property.
     *
     * @param value allowed object is
     *              {@link Domain.Projects }
     */
    public void setProjects(Domain.Projects value) {
        this.projects = value;
    }

    /**
     * Gets the value of the tasks property.
     *
     * @return possible object is
     * {@link Domain.Tasks }
     */
    public Domain.Tasks getTasks() {
        return tasks;
    }

    /**
     * Sets the value of the tasks property.
     *
     * @param value allowed object is
     *              {@link Domain.Tasks }
     */
    public void setTasks(Domain.Tasks value) {
        this.tasks = value;
    }

    /**
     * Gets the value of the users property.
     *
     * @return possible object is
     * {@link Domain.Users }
     */
    public Domain.Users getUsers() {
        return users;
    }

    /**
     * Sets the value of the users property.
     *
     * @param value allowed object is
     *              {@link Domain.Users }
     */
    public void setUsers(Domain.Users value) {
        this.users = value;
    }


    /**
     * &lt;p&gt;Java class for anonymous complex type.
     * <p>
     * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
     * <p>
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     * &amp;lt;complexContent&amp;gt;
     * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     * &amp;lt;sequence&amp;gt;
     * &amp;lt;element name="project" type="{http://endpoint.server.tm.aushakov.ru/}project" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     * &amp;lt;/sequence&amp;gt;
     * &amp;lt;/restriction&amp;gt;
     * &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "project"
    })
    public static class Projects {

        protected List<Project> project;

        /**
         * Gets the value of the project property.
         * <p>
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the project property.
         * <p>
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         * getProject().add(newItem);
         * &lt;/pre&gt;
         * <p>
         * <p>
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link Project }
         */
        public List<Project> getProject() {
            if (project == null) {
                project = new ArrayList<Project>();
            }
            return this.project;
        }

    }


    /**
     * &lt;p&gt;Java class for anonymous complex type.
     * <p>
     * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
     * <p>
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     * &amp;lt;complexContent&amp;gt;
     * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     * &amp;lt;sequence&amp;gt;
     * &amp;lt;element name="task" type="{http://endpoint.server.tm.aushakov.ru/}task" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     * &amp;lt;/sequence&amp;gt;
     * &amp;lt;/restriction&amp;gt;
     * &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "task"
    })
    public static class Tasks {

        protected List<Task> task;

        /**
         * Gets the value of the task property.
         * <p>
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the task property.
         * <p>
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         * getTask().add(newItem);
         * &lt;/pre&gt;
         * <p>
         * <p>
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link Task }
         */
        public List<Task> getTask() {
            if (task == null) {
                task = new ArrayList<Task>();
            }
            return this.task;
        }

    }


    /**
     * &lt;p&gt;Java class for anonymous complex type.
     * <p>
     * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
     * <p>
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     * &amp;lt;complexContent&amp;gt;
     * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     * &amp;lt;sequence&amp;gt;
     * &amp;lt;element name="user" type="{http://endpoint.server.tm.aushakov.ru/}adaptedUser" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     * &amp;lt;/sequence&amp;gt;
     * &amp;lt;/restriction&amp;gt;
     * &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "user"
    })
    public static class Users {

        protected List<AdaptedUser> user;

        /**
         * Gets the value of the user property.
         * <p>
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the user property.
         * <p>
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         * getUser().add(newItem);
         * &lt;/pre&gt;
         * <p>
         * <p>
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AdaptedUser }
         */
        public List<AdaptedUser> getUser() {
            if (user == null) {
                user = new ArrayList<AdaptedUser>();
            }
            return this.user;
        }

    }

}
