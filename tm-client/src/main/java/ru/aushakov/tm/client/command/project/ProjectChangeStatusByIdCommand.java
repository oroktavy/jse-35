package ru.aushakov.tm.client.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractProjectCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.enumerated.Status;
import ru.aushakov.tm.client.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.PROJECT_CHANGE_STATUS_BY_ID;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change project status by id";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS " + Arrays.toString(Status.values()) + ":");
        @Nullable final String statusId = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getProjectService()
                .getProjectEndpointPort().changeProjectStatusById(id, statusId, session))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
