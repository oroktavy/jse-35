package ru.aushakov.tm.client.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractProjectCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Project;
import ru.aushakov.tm.client.endpoint.ProjectEndpoint;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.enumerated.SortType;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.PROJECT_LIST;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        System.out.println("[PROJECT LIST]");
        @Nullable List<Project> projects;
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + " OR PRESS ENTER:");
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final ProjectEndpoint projectService = serviceLocator.getProjectService().getProjectEndpointPort();
        projects = projectService.findAllSortedProjects(session, sortType);
        if (projects == null || projects.size() < 1) {
            System.out.println("[NOTHING FOUND]");
            return;
        }
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
