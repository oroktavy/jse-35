package ru.aushakov.tm.client.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.client.command.AbstractCommand;
import ru.aushakov.tm.client.constant.ArgumentConst;
import ru.aushakov.tm.client.constant.TerminalConst;

public final class AboutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_ABOUT;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ArgumentConst.ARG_ABOUT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show general application info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: " + Manifests.read("developer"));
        System.out.println("E-MAIL: " + Manifests.read("email"));
    }

}
