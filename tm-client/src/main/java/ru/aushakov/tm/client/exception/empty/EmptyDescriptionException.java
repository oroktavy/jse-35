package ru.aushakov.tm.client.exception.empty;

public class EmptyDescriptionException extends RuntimeException {

    public EmptyDescriptionException() {
        super("Provided description is empty!");
    }

}
