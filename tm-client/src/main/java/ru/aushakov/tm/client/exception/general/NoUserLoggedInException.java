package ru.aushakov.tm.client.exception.general;

public class NoUserLoggedInException extends RuntimeException {

    public NoUserLoggedInException() {
        super("No user's currently logged in!");
    }

}
