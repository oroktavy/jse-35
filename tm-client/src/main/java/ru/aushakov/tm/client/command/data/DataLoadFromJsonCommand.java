package ru.aushakov.tm.client.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.command.AbstractDataCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.AdminEndpoint;
import ru.aushakov.tm.client.endpoint.Domain;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.ConfigProperty;
import ru.aushakov.tm.client.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataLoadFromJsonCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_LOAD_FROM_JSON;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load application data from json format";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM JSON]");
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final Session session = authService.getSession();
        @NotNull final AdminEndpoint dataService = serviceLocator.getDataService().getAdminEndpointPort();
        @NotNull final String fileJson = dataService.getDataFileName(ConfigProperty.DATAFILE_JSON.name(), session);
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(fileJson)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        dataService.setDomain(domain, session);
        authService.setSession(null);
    }

}
