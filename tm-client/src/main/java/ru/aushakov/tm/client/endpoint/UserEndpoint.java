package ru.aushakov.tm.client.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.3
 * 2021-06-28T09:49:57.040+03:00
 * Generated source version: 3.4.3
 */
@WebService(targetNamespace = "http://endpoint.server.tm.aushakov.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.server.tm.aushakov.ru/UserEndpoint/viewProfileRequest", output = "http://endpoint.server.tm.aushakov.ru/UserEndpoint/viewProfileResponse")
    @RequestWrapper(localName = "viewProfile", targetNamespace = "http://endpoint.server.tm.aushakov.ru/", className = "ru.aushakov.tm.client.endpoint.ViewProfile")
    @ResponseWrapper(localName = "viewProfileResponse", targetNamespace = "http://endpoint.server.tm.aushakov.ru/", className = "ru.aushakov.tm.client.endpoint.ViewProfileResponse")
    @WebResult(name = "return", targetNamespace = "")
    public AdaptedUser viewProfile(

            @WebParam(name = "session", targetNamespace = "")
                    Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.server.tm.aushakov.ru/UserEndpoint/changePasswordRequest", output = "http://endpoint.server.tm.aushakov.ru/UserEndpoint/changePasswordResponse")
    @RequestWrapper(localName = "changePassword", targetNamespace = "http://endpoint.server.tm.aushakov.ru/", className = "ru.aushakov.tm.client.endpoint.ChangePassword")
    @ResponseWrapper(localName = "changePasswordResponse", targetNamespace = "http://endpoint.server.tm.aushakov.ru/", className = "ru.aushakov.tm.client.endpoint.ChangePasswordResponse")
    public void changePassword(

            @WebParam(name = "oldPassword", targetNamespace = "")
                    java.lang.String oldPassword,
            @WebParam(name = "newPassword", targetNamespace = "")
                    java.lang.String newPassword,
            @WebParam(name = "session", targetNamespace = "")
                    Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.server.tm.aushakov.ru/UserEndpoint/addUserRequest", output = "http://endpoint.server.tm.aushakov.ru/UserEndpoint/addUserResponse")
    @RequestWrapper(localName = "addUser", targetNamespace = "http://endpoint.server.tm.aushakov.ru/", className = "ru.aushakov.tm.client.endpoint.AddUser")
    @ResponseWrapper(localName = "addUserResponse", targetNamespace = "http://endpoint.server.tm.aushakov.ru/", className = "ru.aushakov.tm.client.endpoint.AddUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public AdaptedUser addUser(

            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password,
            @WebParam(name = "email", targetNamespace = "")
                    java.lang.String email,
            @WebParam(name = "session", targetNamespace = "")
                    Session session
    );
}
