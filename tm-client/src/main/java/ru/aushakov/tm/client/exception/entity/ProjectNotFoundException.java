package ru.aushakov.tm.client.exception.entity;

public class ProjectNotFoundException extends RuntimeException {

    public ProjectNotFoundException() {
        super("Project not found!");
    }

}
