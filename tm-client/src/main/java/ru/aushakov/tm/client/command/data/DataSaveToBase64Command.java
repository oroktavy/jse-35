package ru.aushakov.tm.client.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractDataCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.AdminEndpoint;
import ru.aushakov.tm.client.endpoint.Domain;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.ConfigProperty;
import ru.aushakov.tm.client.enumerated.Role;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public final class DataSaveToBase64Command extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_SAVE_TO_BASE64;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save application data to base64 format";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE TO BASE64]");
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        @NotNull final AdminEndpoint dataService = serviceLocator.getDataService().getAdminEndpointPort();
        @NotNull final Domain domain = dataService.getDomain(session);
        @NotNull final String fileBase64 = dataService.getDataFileName(ConfigProperty.DATAFILE_BASE64.name(), session);
        @NotNull final File file = new File(fileBase64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.close();
    }

}
