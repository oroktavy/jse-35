package ru.aushakov.tm.client.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.command.AbstractDataCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.AdminEndpoint;
import ru.aushakov.tm.client.endpoint.Domain;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.ConfigProperty;
import ru.aushakov.tm.client.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataLoadFromBinaryCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_LOAD_FROM_BINARY;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load application data from binary format";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM BINARY]");
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final Session session = authService.getSession();
        @NotNull final AdminEndpoint dataService = serviceLocator.getDataService().getAdminEndpointPort();
        @NotNull final String fileBinary = dataService.getDataFileName(ConfigProperty.DATAFILE_BINARY.name(), session);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(fileBinary);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        dataService.setDomain(domain, session);
        objectInputStream.close();
        fileInputStream.close();
        authService.setSession(null);
    }

}
